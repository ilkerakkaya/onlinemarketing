# onlinemarketing

Graduation project of LiveCodingOnline.

## Master 
    Production
## Develop 
    Staging

## Road Map
First of all you need to clone the project to your local environment.
Get latest version of the project with **git pull**, then create your own feature branch to work.
After you finished your work push your code and create a PR to develop. 
Assign at least one mentor and one of your classmate as reviewer. 
After you get two approval to your pull request, you can merge your code to the develop. 
After each sprint we are going to release the app with the new version.